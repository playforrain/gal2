﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GAl2
{
    class IntermediateCrossover : ICrossover
    {
        readonly Generator _generator;
        ValueTuple<double, double> _scope;

        public IntermediateCrossover(Generator generator, ValueTuple<double, double> scope)
        {
            _generator = generator;
            _scope = scope;
        }       

        public (List<double>, List<double>) Do(List<double> parent1, List<double> parent2)
        {
            //-d, d + 1
            var d = 0.25;           
            var coef = (-d, d + 1);

            var newborn1Genes = new List<double>();
            var newborn2Genes = new List<double>();

            //O1=𝑃1+𝑎1∙(𝑃2−𝑃1),O2=𝑃1+𝑎2∙(𝑃2−𝑃1)
            for (int i = 0; i < parent1.Count; i++)
            {
                
                double o1;               
                do
                {
                    var a1 = _generator.NextDouble(coef.Item1, coef.Item2);
                    o1 = parent1[i] + a1 * (parent2[i] - parent1[i]);

                } while (!(_scope.Item1 < o1 && o1 < _scope.Item2));
                                    
                newborn1Genes.Add(o1);

                double o2;
                do
                {
                    var a2 = _generator.NextDouble(coef.Item1, coef.Item2);
                    o2 = parent1[i] + a2 * (parent2[i] - parent1[i]);

                } while (!(_scope.Item1 < o2 && o2 < _scope.Item2));

                newborn2Genes.Add(o2);
            }


            return (newborn1Genes, newborn2Genes);
        }
    }
}
