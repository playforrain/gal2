﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections;

namespace GAl2
{
    /// <summary>        
    /// 5 вариант
    /// https://www.sfu.ca/~ssurjano/rosen.html
    /// 
    /// 7 вариант
    /// https://www.sfu.ca/~ssurjano/schwef.html
    /// 
    /// 8 вариант
    /// https://www.sfu.ca/~ssurjano/griewank.html
    /// </summary>
    class Program
    {
        static readonly Generator _generator = new Generator();
      
        static void Main(string[] args)
        {
            //1.4372
            //var res = Func.Griewank(new []{ 30.0, 22.0, -9.0 }.ToList());
            //var configuration = new Configuration(
            //    500,
            //    0.5f,
            //    0.01f,
            //    Func.Griewank,
            //    (-600f, 600f),
            //    400);

            //171.26
            //var res = Func.Rosenbrock(new List<double>() { 1.0, -0.3, 0.0145 }.ToList());

            var configuration = new Configuration(
                500,
                0.5f,
                0.01f,
                Func.Rosenbrock,
                (-2.048f, 2.048f),
                4);

            //798.589
            //var res = Func.Schwefel(new []{ 420.9687, 50, 5 }.ToList());

            //458.3597
            //var res = Func.Schwefel2(new[] { 420.9687, 50, 5 }.ToList());

            //var configuration = new Configuration(
            //    500,
            //    0.5f,
            //    0.01f,
            //    Func.Schwefel,
            //    (-500.0f, 500.0f),
            //    400);

            var population = new Population(_generator, configuration, null);

            var genes = population
                .Reproduction()
                .Select()
                .Crossingover()
                .Mutation()
                .Reduction();

            Console.WriteLine();
            Console.WriteLine($"====GENERATION 1=====");

            foreach (var item in genes)
                Console.Write($"{item.YValue} ");

            Console.WriteLine();

            var counter = 1;
            
            while (true)
            {
                Console.WriteLine();
                Console.WriteLine($"====GENERATION {++counter}===FF={population.YAverageValue}==");

                population = new Population(_generator, configuration, genes);

                genes = population
                    .Reproduction()
                    .Select()
                    .Crossingover()
                    .Mutation()
                    .Reduction();

                foreach (var item in genes)
                    Console.Write($"{item.YValue} ");

                Console.WriteLine();
                Console.WriteLine();

                var key = Console.ReadKey(true);

                if (key.KeyChar == 'q')
                    break;

            }

            var best =population.GetBestChromosomeGenesValues();

            Console.WriteLine("Значения генов лучшей хромосомы:");

            foreach (var item in best)
                Console.WriteLine(item);

            Console.WriteLine();

            Console.WriteLine("Нажмите и завершите...");

            Console.ReadKey();
        }
    }
}
