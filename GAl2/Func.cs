﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GAl2
{
    class Func
    {
        //d = length(xx);
        //sum = 0;
        //prod = 1;

        //for ii = 1:d

        //    xi = xx(ii);
        //    sum = sum + xi ^ 2 / 4000;
        //    prod = prod * cos(xi / sqrt(ii));
        //end

        //y = sum - prod + 1;

        //end

        public static readonly Func<List<double>, double> Griewank = (xx) =>
        {
            var d = xx.Count;
            var sum = 0.0;
            var prod = 1.0;
            for (int ii = 1; ii <= xx.Count; ii++)
            {
                var xi = xx[ii - 1];
                sum = sum + Math.Pow(xi, 2) / 4000;
                prod = prod * Math.Cos(xi / Math.Sqrt(ii));
            }

            return sum - prod + 1;
        };


        /*
        d = length(xx);
        sum = 0;
        for ii = 1:(d-1)
            xi = xx(ii);
            xnext = xx(ii+1);
            new = 100*(xnext-xi^2)^2 + (xi-1)^2;
            sum = sum + new;
        end

        y = sum;

        end
         */
        public static readonly Func<List<double>, double> Rosenbrock = (xx) =>
        {
            var d = xx.Count;
            var sum = 0.0;           
            for (int ii = 1; ii < xx.Count; ii++)
            {
                var xi = xx[ii - 1];
                var xnext = xx[ii];
                var new_ = 100 * Math.Pow(xnext - Math.Pow(xi, 2),2) + Math.Pow((xi - 1),2);
                sum = sum + new_;
            }

            return sum;
        };

        /*
        d = length(xx);
        sum = 0;
        for ii = 1:d
	        xi = xx(ii);
	        sum = sum + xi*sin(sqrt(abs(xi)));
        end

        y = 418.9829*d - sum;

        end
         */
        public static readonly Func<List<double>, double> Schwefel = (xx) =>
        {
            var d = xx.Count;
            var sum = 0.0;
            for (int ii = 1; ii <= xx.Count; ii++)
            {
                var xi = xx[ii - 1];                
                sum = sum + xi * Math.Sin(Math.Sqrt(Math.Abs(xi)));
            }

            return 418.9829 * d - sum;
        };

        //вариант методички
        public static readonly Func<List<double>, double> Schwefel2 = (xx) =>
        {
            var d = xx.Count;
            var sum = 0.0;
            for (int ii = 1; ii <= xx.Count; ii++)
            {
                var xi = xx[ii - 1];
                sum = sum + xi * Math.Sin(Math.Sqrt(Math.Abs(xi)));
            }

            return sum;
        };
    }
}
