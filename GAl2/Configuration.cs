﻿using System;
using System.Collections.Generic;

namespace GAl2
{
    class Configuration
    {
        public Configuration(int populationPower, float crossingoverPropability,
            float mutationPropability, Func<List<double>, double> fitnessFunc,
            ValueTuple<double, double> scope,
            int step)
        {
            PopulationPower = populationPower;
            CrossingoverPropability = crossingoverPropability;
            MutationPropability = mutationPropability;
            FitnessFunc = fitnessFunc;
            Scope = scope;
            Step = step;
        }

        public int PopulationPower { get; private set; }

        public float CrossingoverPropability { get; private set; }

        public float MutationPropability { get; private set; }

        public Func<List<double>, double> FitnessFunc { get; private set; }

        public ValueTuple<double, double> Scope { get; private set; }

        public int Step { get; private set; }
    }
}
