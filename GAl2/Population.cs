﻿using System.Linq;
using System.Collections.Generic;
using System;

namespace GAl2
{
    class Population
    {
        readonly Generator _generator;

        readonly Configuration _configuration;

        readonly ICrossover _crossover;

        readonly Collector _collector = new Collector();

        public double YAverageValue { get; private set; }

        public double YMinValue { get; private set; }

        List<Chromosome> population;

        readonly List<Chromosome> selectedParents = new List<Chromosome>();

        readonly List<Tuple<Chromosome, Chromosome>> marriedParents = new List<Tuple<Chromosome, Chromosome>>();
        
        public Population(Generator generator, Configuration configuration, Chromosome[] genes)            
        {
            _generator = generator;
            _configuration = configuration;
            //_crossover = new DiscreateCrossover(_generator);
            _crossover = new IntermediateCrossover(_generator, _configuration.Scope);

            if (genes == null)
            {
                population = new List<Chromosome>();

                for (int i = 0; i < _configuration.PopulationPower; i++)
                {
                    var candidate = new Chromosome(_generator, _configuration.FitnessFunc, _configuration.Scope,
                        _configuration.Step);
                    
                    while (population.Any(_ => Enumerable.SequenceEqual(_.Genes, candidate.Genes)))
                        candidate = new Chromosome(_generator, _configuration.FitnessFunc, _configuration.Scope,
                        _configuration.Step);


                    population.Add(candidate);                    
                }

                _collector.Collect(population);
            }
            else
                population = genes.ToList();

            double accum = 0;

            var maxYValue = population.Max(__ => __.YValue);

            /*
                * для нахождения вероятности берем сумму абсолютных значений фитнес функции.
                * при неизвестном максимуме функции за верхнюю границу берем максимальное
                * значение фитнес функции в популяции
                */
            for (int i = 0; i < _configuration.PopulationPower; i++)
            {
                population[i].SetPValue(maxYValue, population.Sum(_ => Math.Abs(_.YValue - maxYValue)));                
                population[i].SetMValue(_configuration.PopulationPower);
                accum += population[i].YValue;               
            }

            YMinValue = population.Min(_ => _.YValue);

            YAverageValue = accum / _configuration.PopulationPower;            
        }       
        
        public Population Reproduction()
        {

            for (int i = 0; i < _configuration.PopulationPower; i++)
            {
                var count = (int)Math.Round(population[i].MValue);
                for (int j = 0; j < count; j++)
                        selectedParents.Add(population[i]);                                  
            }

            return this;
        }

        public Population Select()
        {
            //список отобранных для кроссинговера особей не пустой
            //и в нем содержиться более одной уникальной особи
            var checkGenes = selectedParents?.FirstOrDefault()?.Genes;
            if (checkGenes == null)
                throw new Exception("no future  parents");

            if (selectedParents.All(_ => Enumerable.SequenceEqual(_.Genes, checkGenes)))
                throw new Exception("only one parent in set");            

            do
            {
                var fatherIndex = _generator.Next(0, selectedParents.Count);
                var father = selectedParents.ToList()[fatherIndex];

                var motherIndex = _generator.Next(0, selectedParents.Count);
                var mother = selectedParents.ToList()[motherIndex];

                //while (fatherIndex == motherIndex || Enumerable.SequenceEqual(father.Genes, mother.Genes))
                while (fatherIndex == motherIndex || father.YValue == mother.YValue)
                {
                    motherIndex = _generator.Next(0, selectedParents.Count);
                    mother = selectedParents.ToList()[motherIndex];
                }
                    
                marriedParents.Add(Tuple.Create(father, mother));
            } while (marriedParents.Count < _configuration.PopulationPower / 2);                      

            return this;
        }


        public Population Crossingover()
        {                                   
            foreach (var entry in marriedParents)
            {
                /*
                 * если true, событие произошло и делаем кроссинговер,
                 * если false, кроссинговер не происходит, в след. поколение идут сами родители
                 */                                             
                if (_generator.GetProbability(_configuration.CrossingoverPropability))
                {
                    Chromosome newborn1, newborn2;
                    do
                    {
                        var (newborn1Genes, newborn2Genes) = _crossover.Do(entry.Item1.Genes, entry.Item2.Genes);

                        newborn1 = new Chromosome(_generator, newborn1Genes.ToArray(),
                            _configuration.FitnessFunc, _configuration.Scope);

                        newborn2 = new Chromosome(_generator, newborn2Genes.ToArray(),
                        _configuration.FitnessFunc, _configuration.Scope);

                        
                    } while (new [] { entry.Item1, entry.Item2 }
                        .Intersect(new[] { newborn1,newborn2}).Any());

                    population.Add(newborn1);
                    population.Add(newborn2);

                }                                             
            }

            return this;
        }        

        public Population Mutation()
        {
            if (_generator.GetProbability(_configuration.MutationPropability))
            {
                var mutantIndex = _generator.Next(0, population.Count - 1);
                var mutantGenes = population[mutantIndex].Genes;
                var mutantGeneIndex = _generator.Next(0, mutantGenes.Count - 1);
                mutantGenes[mutantGeneIndex] = _generator.NextDouble(_configuration.Scope.Item1, 
                    _configuration.Scope.Item2);
                var mutant = new Chromosome(_generator, mutantGenes.ToArray(), _configuration.FitnessFunc, 
                    _configuration.Scope);
                population[mutantIndex] = mutant;
            }      
            
            return this;
        }        
        
        public Chromosome[] Reduction()
        {
            var goodByeCount = population.Count - _configuration.PopulationPower;

            population = population.OrderBy(_ => _.YValue).ToList();

            population.RemoveRange(population.Count - goodByeCount, goodByeCount);

            _collector.Collect(population);

            return population.ToArray();
        }

        public IReadOnlyCollection<string> GetBestChromosomeGenesValues() =>
            population.First().Genes.Select(_ => _.ToString()).ToList();
    }
}
