﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace GAl2
{
    [DebuggerDisplay("Y={YValue} P={PValue} M={MValue}")]
    class Chromosome
    {
        readonly Generator _generator;
        readonly ValueTuple<double, double> _scope;

        public Chromosome(Generator generator, Func<List<double>, double> fitnessFunc,
            ValueTuple<double, double> scope)
        {
            _generator = generator ?? throw new ArgumentNullException("generator");
            FitnessFunc = fitnessFunc ?? throw new ArgumentNullException("fitnessFunc");

            if (scope.Item1 >= scope.Item2)
                throw new ArgumentException("scope");
            _scope = scope;            
        }

        public Chromosome(Generator generator, Func<List<double>, double> fitnessFunc,
           ValueTuple<double, double> scope, int step) 
            : this(generator, fitnessFunc, scope)
        {
            for (int i = 0; i < (_scope.Item2 - _scope.Item1) / step; i++)
                Genes.Add(_generator.NextDouble(scope.Item1, scope.Item2));

            YValue = Math.Round(FitnessFunc.Invoke(Genes),4);
        }

        public Chromosome(Generator generator, double[] genes, Func<List<double>, double> fitnessFunc,
           ValueTuple<double, double> scope)
            : this(generator, fitnessFunc, scope)
        {
            Genes = genes.ToList();

            YValue = Math.Round(FitnessFunc.Invoke(Genes),4);
        }

        public Guid Id { get; private set; } = Guid.NewGuid();

        public List<double> Genes { get; private set; } = new List<double>();     

        public double YValue { get; private set; }

        public double PValue { get; private set; }

        public double MValue { get; private set; }

        Func<List<double>, double> FitnessFunc;
        
        public void SetPValue(double populationMaxYValue, double totalYValues) => PValue = 
            Math.Abs(YValue - populationMaxYValue) / totalYValues;

        public void SetMValue(int nValue) => MValue = PValue * nValue;           
    }
}
