﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GAl2
{
    interface ICrossover
    {       
        (List<double>, List<double>) Do(List<double> parent1, List<double> parent2);
    }
}
