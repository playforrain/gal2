﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GAl2
{
    class DiscreateCrossover : ICrossover
    {
        readonly Generator _generator;

        public DiscreateCrossover(Generator generator) => _generator = generator;
        
        public (List<double>, List<double>) Do(List<double> parent1, List<double> parent2)
        {
            var newborn1Genes = new List<double>();

            for (int i = 0; i < parent1.Count; i++)
            {
                if (_generator.GetProbability(0.5f))
                    newborn1Genes.Add(parent1[i]);
                else
                    newborn1Genes.Add(parent2[i]);
            }

            var newborn2Genes = new List<double>();

            for (int i = 0; i < parent1.Count; i++)
            {
                if (_generator.GetProbability(0.5f))
                    newborn2Genes.Add(parent1[i]);
                else
                    newborn2Genes.Add(parent2[i]);
            }

            return (newborn1Genes, newborn2Genes);
        }
    }
}
