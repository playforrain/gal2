﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;

namespace GAl2
{
    class Collector
    {
        const string Where = "data.txt";

        public void Collect(IReadOnlyCollection<Chromosome> population)
        {           
            var line = string.Join(" ",  population.Select(chromosome => $"[{string.Join(" ", chromosome.Genes)}]=>" +
            $"{chromosome.YValue}")) + Environment.NewLine;

            File.AppendAllText(Where, line);            
        }
    }
}
